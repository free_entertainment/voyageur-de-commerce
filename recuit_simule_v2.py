import random
import numpy as np
import math

class Graphe:
  def __init__(self, villes):
    self.villes = villes
  def get_distance_totale(self):
    i = 1 
    res = 0
    for v in self.villes:
      res = res + self.distance_between_two_nodes(v.get_latitude(), v.get_longitude(),
                                             self.villes[i].get_latitude(), 
                                             self.villes[i].get_longitude())
      if i == 19:
        break;
      i = i + 1
    res = res + self.distance_between_two_nodes(self.villes[i].get_latitude(),
                                           self.villes[i].get_longitude(),
                                           self.villes[0].get_latitude(),
                                           self.villes[0].get_longitude())
    return res

  def swap_nodes(self, n1, n2):
    temp = self.villes[n1]
    self.villes[n1] = self.villes[n2]
    self.villes[n2] = temp

  def get_node(self, n):
    return self.villes[n]

  def get_size_graphe(self):
    return len(self.villes)

  def print_graphe(self):
    i = 0
    for v in self.villes:
      print(v.get_name())

  def distance_between_two_nodes(self, lat, lon, lat2, lon2):
    R = 6373.0
    dlon = lon2 - lon
    dlat = lat2 - lat

    a = math.sin(dlat / 2)**2 + math.cos(lat) * math.cos(lat2) * math.sin(dlon / 2)**2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    distance = R * c
    return distance

class Node:
  def __init__(self, nom, latitude, longitude):
    self.nom = nom 
    self.latitude = latitude
    self.longitude = longitude
  def get_latitude(self):
    return self.latitude
  def get_longitude(self):
    return self.longitude
  def get_name(self):
    return self.nom


if __name__ == "__main__":
  Paris = Node("Paris", math.radians(48.864716), math.radians(2.349014))
  Lille = Node("Lille",math.radians(50.629250), math.radians(3.057256))
  Reims = Node("Reims", math.radians(49.262798), math.radians(4.034700))
  Marseille = Node("Marseille", math.radians(43.296398), math.radians(5.370000))
  Lyon = Node("Lyon", math.radians(45.763420), math.radians(4.834277))
  Toulouse =  Node("Toulouse", math.radians(43.604500), math.radians(1.444000))
  Nice = Node("Nice", math.radians(43.675819), math.radians(7.289429))
  Nantes = Node('Nantes', math.radians(47.218102), math.radians(-1.552800))
  Montpellier = Node('Montpellier', math.radians(43.611900), math.radians(3.877200))
  Strasbourg = Node('Strasbourg', math.radians(48.580002), math.radians(7.750000))
  Bordeaux =   Node('Bordeaux', math.radians(44.836151), math.radians(-0.580816))
  Rennes = Node('Rennes', math.radians(48.114700), math.radians(-1.679400))
  Saint_Etienne = Node('Saint-Etienne', math.radians(45.434700), math.radians(4.390300))
  Toulon = Node('Toulon', math.radians(43.125832), math.radians(5.930556))
  Grenoble = Node('Grenoble', math.radians(45.171547), math.radians(5.722387))
  Nimes = Node('Nimes', math.radians(43.838001), math.radians(4.361000))
  Angers = Node('Angers', math.radians(47.473614), math.radians(-0.554167))
  Villeurbanne = Node('Villeurbanne', math.radians(45.766701), math.radians(4.880300))
  Dijon = Node('Dijon',math.radians(47.316667), math.radians(5.016667))
  Le_Havre = Node('Le Havre', math.radians(49.490002), math.radians(0.100000))

  Villes = Graphe([Paris, Lille, Reims, Marseille, Lyon, Toulouse, Nice, 
                   Nantes, Montpellier, Strasbourg,  Bordeaux,
                   Rennes, Saint_Etienne, Toulon, Grenoble, Nimes, Angers, 
                   Villeurbanne, Dijon, Le_Havre])

  cost0 = Villes.get_distance_totale()
  T = 50
  factor = 0.99
  T_init = T

  for i in range(1000):
    T = T * factor
    for j in range(500):
      r1 = random.randint(0, Villes.get_size_graphe() - 1) 
      r2 = random.randint(0, Villes.get_size_graphe() - 1) 
      Villes.swap_nodes(r1, r2)

      cost1 = Villes.get_distance_totale()
      if cost1 < cost0:
        cost0 = cost1
      else:
        x = np.random.uniform()
        if x < np.exp((cost0 - cost1) / T):
          cost0 = cost1
        else:
          Villes.swap_nodes(r1, r2)
    if T < 1:
      break;

print("La distance min est de " + str(cost0))
Villes.print_graphe()
